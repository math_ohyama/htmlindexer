#!/bin/sh
export INDEXER_HOME=$HOME/search/news-indexer

# delete index directory
rm -rf /usr/local/apache2/docs/houjin.int.twmu.ac.jp/intra/web-news/search/news-search/index

# make index
java -cp $INDEXER_HOME/build/classes/:$INDEXER_HOME/lib/commons-configuration-1.9.jar:$INDEXER_HOME/lib/commons-lang-2.6.jar:$INDEXER_HOME/lib/commons-logging-1.1.3.jar:$INDEXER_HOME/lib/javaxt-core.jar:$INDEXER_HOME/lib/jsoup-1.7.2.jar:$INDEXER_HOME/lib/lucene-core-2.0.0.jar:$INDEXER_HOME/lib/lucene-ja.jar:$INDEXER_HOME/lib/pdfbox-app-1.8.2.jar:$INDEXER_HOME/lib/tika-app-1.4.jar jp.co.studioegg.twmu.news.cmd.IndexJHTML

#!/bin/sh
chmod -R 777 /usr/local/apache2/docs/houjin.int.twmu.ac.jp/intra/web-news/search/news-search/index
