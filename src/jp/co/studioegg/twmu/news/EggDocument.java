/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.studioegg.twmu.news;

import java.io.File;
import jp.co.studioegg.twmu.news.utils.ConfigurationFactory;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.lucene.document.DateTools;

/**
 *
 * @author math
 */
public abstract class EggDocument {

	protected static PropertiesConfiguration config = ConfigurationFactory.getInstance();
	static char dirSep = System.getProperty("file.separator").charAt(0);

	public static String uid(File f) {
		// Append path and date into a string in such a way that lexicographic
		// sorting gives the same results as a walk of the file hierarchy. Thus
		// null (\u0000) is used both to separate directory components and to
		// separate the path from the date.
		return f.getPath().replace(HtmlDocument.dirSep, '\u0000') + "\u0000" + DateTools.timeToString(f.lastModified(), DateTools.Resolution.SECOND);
	}

	public static String uid2url(String uid) {
		String url = uid.replace('\u0000', '/'); // replace nulls with slashes
		return url.substring(0, url.lastIndexOf('/')); // remove date from end
	}
}
