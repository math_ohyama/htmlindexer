/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.studioegg.twmu.news.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author math
 */
public class CommonHelper {

	public static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	private static final Log log = LogFactory.getLog(CommonHelper.class);

	public static String getSuffix(String fileName) {
		if (fileName == null) {
			return null;
		}
		int point = fileName.lastIndexOf(".");
		if (point != -1) {
			return fileName.substring(point + 1).toLowerCase();
		}
		return fileName.toLowerCase();
	}

	/**
	 * Date型のオブジェクトをString型に変換します.
	 */
	public static String date2Str(java.util.Date date) {
		if (date == null) {
			return null;
		}
		return (new SimpleDateFormat(DATE_PATTERN)).format(date);
	}

	/**
	 * String型のオブジェクトをDate型に変換します.
	 */
	public static java.util.Date str2Date(String source) {
		if (StringUtils.isEmpty(source)) {
			return null;
		}
		try {
			return (new SimpleDateFormat(DATE_PATTERN)).parse(source);
		} catch (ParseException e) {
			log.error(e.getLocalizedMessage(), e);
			return null;
		}
	}
}
