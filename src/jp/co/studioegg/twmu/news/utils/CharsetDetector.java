package jp.co.studioegg.twmu.news.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * This Class detect Japanese Charset such as EUC-JP, ISO-2022-JP, Shift_JIS. It is implemented the algorithm by
 * describing following URL:
 * <pre>
 * [Hey! Java Programming! //Japanese//]
 * http://www.mars.dti.ne.jp/~torao/program/appendix/japanese.html
 * </pre>
 */
public class CharsetDetector {

	static String JIS = "ISO-2022-JP";
	static String SJIS = "Shift_JIS";
	static String EUCJP = "EUC_JP";
	static String ASCII = "ISO-8859-1";
	static String UTF8 = "UTF-8";
	static String unknown = UTF8;
	static int BUFFER_SIZE = 10000;
	private InputStream in = null;
	private boolean prefetchFlag = false;
	private int prefetchChar;
	private int length = 0;
	private int cnt = 0;

	/**
	 * Constructor.
	 *
	 * @param fname file name.
	 */
	public CharsetDetector(String fname) throws IOException {
		File file = new File(fname);
		length = (int) file.length();
		this.in = new BufferedInputStream(new FileInputStream(file),
						BUFFER_SIZE);
	}

	/**
	 * Constructor. CAUTION: This method may don't work correctly.
	 *
	 * @param in Stream.
	 */
	public CharsetDetector(InputStream in) throws IOException {
		length = in.available();
		this.in = new BufferedInputStream(in, BUFFER_SIZE);
	}

	/**
	 * Constructor.
	 *
	 * @param file file.
	 */
	public CharsetDetector(File file) throws IOException {
		length = (int) file.length();
		this.in = new BufferedInputStream(new FileInputStream(file),
						BUFFER_SIZE);
	}

	/**
	 * setter for unknown charset.
	 *
	 * @param enc charset for unkown file.
	 */
	public static void setUnknownCharset(String enc) {
		unknown = enc;
	}

	int nextChar() throws IOException {
		if (cnt == length) {
			return -1;
		}
		cnt++;
		try {
			if (prefetchFlag == false) {
				int tmp = in.read();
				//	            System.out.println("readChar="+tmp);
				return tmp;
			} else {
				prefetchFlag = false;
				return prefetchChar;
			}
		} catch (RuntimeException e) {
			throw new IOException(e.toString());
		}
	}

	int prefetchChar() throws IOException {
		if (prefetchFlag) {
			return prefetchChar;
		}
		if (cnt == length) {
			return -1;
		}

		prefetchFlag = true;

		prefetchChar = in.read();
		//        System.out.println("prefetchChar="+prefetchChar);
		return prefetchChar;
	}

	/**
	 * detect charset
	 *
	 * @return charset
	 */
	public String detect() throws IOException {
		int ch;
		while ((ch = prefetchChar()) != -1) {
			if (ch < 0x80) {
				if (ch == 0x1B) {
					return JIS;
				}
			} else {
				if (ch > 0xa0 && ch < 0xe0) {
					return detectEUCSJIS();
				}

			}
			ch = nextChar();
		}
		return ASCII;
	}

	static private boolean isSJIS1(int ch) {
		return ch >= 0x81 && ch <= 0x9f;
	}

	static private boolean isSJIS2(int ch) {
		return (ch >= 0x40 && ch <= 0x7e) || (ch >= 0x80 && ch <= 0xa0);
	}

	static private boolean isEUC1(int ch) {
		return (ch >= 0xf0 && ch <= 0xfe);
	}

	static private boolean isEUC2(int ch) {
		return (ch >= 0xfd && ch <= 0xfe);
	}

	static private boolean isKana(int ch) {
		return (ch >= 0xa1 && ch <= 0xdf);
	}

	static private boolean isASCII(int ch) {
		return (ch >= 0x00 && ch <= 0x7f);
	}

	private String detectEUCSJIS() throws IOException {
		int ch;
		while ((ch = nextChar()) != -1) {
			if (isASCII(ch)) {
				continue;
			}

			if (isSJIS1(ch)) {
				return SJIS;
			}

			if (isKana(ch)) {
				int pre = prefetchChar();
				//                ch = nextChar();
				if (isASCII(pre)
								|| (pre > 0x80 && pre < 0xa0)) {
					return SJIS;
				}
			}
			if (isEUC1(ch)) {
				return EUCJP;
			}

			ch = nextChar();

			if (isSJIS2(ch)) {
				return SJIS;
			} else if (isEUC2(ch)) {
				return EUCJP;
			}
		}
		return unknown;
	}

	public static void main(String args[]) {
		try {
			CharsetDetector cd = new CharsetDetector(args[0]);
			CharsetDetector.setUnknownCharset("EUC_JP");
			System.out.println(cd.detect());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
