/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.studioegg.twmu.news.utils;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author math
 */
public class ConfigurationFactory {

	private static PropertiesConfiguration instance = null;
	private static final Log log = LogFactory.getLog(ConfigurationFactory.class);

	private ConfigurationFactory() {
	}

	public static PropertiesConfiguration getInstance() {
		if (instance == null) {
			try {
				instance = new PropertiesConfiguration("application.properties");
			} catch (ConfigurationException ex) {
				log.error(ex);
				throw new RuntimeException(ex);
			}

		}
		return instance;
	}
}
