/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.studioegg.twmu.news;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import static jp.co.studioegg.twmu.news.EggDocument.uid;
import jp.co.studioegg.twmu.news.utils.CommonHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;

/**
 *
 * @author math
 */
public class PDFDocument {

	private static final Log log = LogFactory.getLog(HtmlDocument.class);

	public static Document Document(File f, String charset) throws IOException, InterruptedException {
		Document doc = new Document();
		doc.add(new Field("type", "pdf", Field.Store.YES, Field.Index.UN_TOKENIZED));
		doc.add(new Field("filename", f.getName(), Field.Store.YES, Field.Index.UN_TOKENIZED));
		String url = HtmlDocument.config.getString("indexer.base.url") + f.getPath().substring(HtmlDocument.config.getString("indexer.html.dir").length());
		doc.add(new Field("url", url, Field.Store.YES, Field.Index.UN_TOKENIZED));
		doc.add(new Field("uid", uid(f), Field.Store.YES, Field.Index.UN_TOKENIZED));
		InputStream is = null;
		try {
			is = new BufferedInputStream(new FileInputStream(f));

			Parser parser = new AutoDetectParser();
			ContentHandler handler = new BodyContentHandler(10*1024*1024);
			Metadata metadata = new Metadata();

			parser.parse(is, handler, metadata, new ParseContext());

			if (metadata.get("Creation-Date") != null) {
				doc.add(new Field("created", DateTools.timeToString(CommonHelper.str2Date(metadata.get("Creation-Date")).getTime(), DateTools.Resolution.MINUTE), Field.Store.YES, Field.Index.UN_TOKENIZED));
			}
			if (metadata.get("modified") != null) {

				doc.add(new Field("updated", DateTools.timeToString(CommonHelper.str2Date(metadata.get("modified")).getTime(), DateTools.Resolution.MINUTE), Field.Store.YES, Field.Index.UN_TOKENIZED));
			}
			doc.add(new Field("content", handler.toString(), Field.Store.YES, Field.Index.TOKENIZED));
			doc.add(new Field("charset", charset, Field.Store.YES, Field.Index.UN_TOKENIZED));
			String title = (StringUtils.isEmpty(metadata.get("title"))) ? "タイトルナシ" : metadata.get("title");
			doc.add(new Field("title", title, Field.Store.YES, Field.Index.TOKENIZED));
			String keywords = (StringUtils.isEmpty(metadata.get("keywords"))) ? "NONE" : metadata.get("title");
			doc.add(new Field("keywords", keywords, Field.Store.YES, Field.Index.UN_TOKENIZED));
			String description = (StringUtils.isEmpty(metadata.get("description"))) ? "NONE" : metadata.get("title");
			doc.add(new Field("description", description, Field.Store.YES, Field.Index.TOKENIZED));
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return doc;
	}
}
