package jp.co.studioegg.twmu.news.cmd;

import java.io.File;
import java.util.Date;
import java.util.List;
import jp.co.studioegg.twmu.news.HtmlDocument;
import jp.co.studioegg.twmu.news.PDFDocument;
import jp.co.studioegg.twmu.news.utils.CommonHelper;
import jp.co.studioegg.twmu.news.utils.ConfigurationFactory;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;

import org.apache.lucene.analysis.cjk.CJKAnalyzer;
import org.apache.lucene.analysis.ja.JapaneseAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;

class IndexJHTML {

	private static boolean deleting = false;	  // true during deletion pass
	private static IndexReader reader;		  // existing index
	private static IndexWriter writer;		  // new index being built
	private static TermEnum uidIter;		  // document id iterator
	private static PropertiesConfiguration config = ConfigurationFactory.getInstance();
	private static final Log log = LogFactory.getLog(IndexJHTML.class);

	public static void main(String[] argv) {
		try {
			String index = config.getString("indexer.index.dir");
			String charset = config.getString("indexer.charset");
			boolean create = config.getBoolean("indexer.create", false);
			File root = new File(config.getString("indexer.html.dir"));
			Date start = new Date();

			if (!create) {				  // delete stale docs
				deleting = true;
				indexDocs(root, index, create, charset);
			}
			Analyzer analyzer = new CJKAnalyzer();
//			Analyzer analyzer = new JapaneseAnalyzer();
			writer = new IndexWriter(index, analyzer, create);
			writer.setMaxFieldLength(1000000);
			indexDocs(root, index, create, charset);	 // add new docs

			log.info("Optimizing index...");
			writer.optimize();
			writer.close();

			Date end = new Date();
			log.info(end.getTime() - start.getTime() + " total milliseconds");

		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
	}

	/* Walk directory hierarchy in uid order, while keeping uid iterator from
	 /* existing index in sync.  Mismatches indicate one of: (a) old documents to
	 /* be deleted; (b) unchanged documents, to be left alone; or (c) new
	 /* documents, to be indexed.
	 */
	private static void indexDocs(File file, String index, boolean create, String charset) throws Exception {

		if (!create) {				  // incrementally update

			reader = IndexReader.open(index);		  // open existing index
			uidIter = reader.terms(new Term("uid", "")); // init uid iterator
			indexDocs(file, charset);
			if (deleting) {				  // delete rest of stale docs
				while (uidIter.term() != null && uidIter.term().field() == "uid") {
					log.info("deleting " + HtmlDocument.uid2url(uidIter.term().text()));
					reader.deleteDocuments(uidIter.term());
					uidIter.next();
				}
				deleting = false;
			}

			uidIter.close();				  // close uid iterator
			reader.close();				  // close existing index

		} else {
			indexDocs(file, charset);
		}
	}

	private static void indexDocs(File file, String charset) throws Exception {
		if (file.isDirectory()) {			  // if a directory
			String[] files = file.list();		  // list its files
			java.util.Arrays.sort(files);			  // sort the files
			for (int i = 0; i < files.length; i++) {	  // recursively index them
				try {
					indexDocs(new File(file, files[i]), charset);
				} catch (Exception e) {
					log.error("fail indexing:" + file.toString() + " : " + e.getLocalizedMessage());
					return;
				}
			}
		} else {
			Boolean isIndexing = false;
			List indexExtensions = config.getList("indexer.indexed.extension");
			for (Object indexExt : indexExtensions) {
				if (CommonHelper.getSuffix(file.getPath()).equals(indexExt.toString())) {
					isIndexing = true;
				}
			}
			List excludeDirs = config.getList("indexer.exclude.dir");
			for (Object extDir : excludeDirs) {
				if (file.getParent().startsWith(extDir.toString())) {
					isIndexing = false;
				}
			}
			if (isIndexing) {
				if (uidIter != null) {
					String uid = HtmlDocument.uid(file);	  // construct uid for doc

					while (uidIter.term() != null && uidIter.term().field() == "uid" && uidIter.term().text().compareTo(uid) < 0) {
						if (deleting) {			  // delete stale docs
							log.info("deleting " + HtmlDocument.uid2url(uidIter.term().text()));
							reader.deleteDocuments(uidIter.term());
						}
						uidIter.next();
					}
					if (uidIter.term() != null && uidIter.term().field() == "uid" && uidIter.term().text().compareTo(uid) == 0) {
						uidIter.next();			  // keep matching docs
					} else if (!deleting) {
						if (CommonHelper.getSuffix(file.getPath()).equals("pdf")) {
							Document doc = PDFDocument.Document(file, charset);
							log.info("adding PDF:" + doc.get("url"));
							writer.addDocument(doc);
						} else {
							Document doc = HtmlDocument.Document(file, charset);
							log.info("adding HTML:" + doc.get("url"));
							writer.addDocument(doc);
						}
					}
				} else {					  // creating a new index
					if (CommonHelper.getSuffix(file.getPath()).equals("pdf")) {
						Document doc = PDFDocument.Document(file, charset);
						log.info("adding PDF:" + doc.get("url"));
						writer.addDocument(doc);
					} else {
						Document doc = HtmlDocument.Document(file, charset);
						log.info("adding HTML:" + doc.get("url"));
						writer.addDocument(doc);
					}
				}
			}
		}
	}
}
