package jp.co.studioegg.twmu.news.cmd;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.analysis.cjk.CJKAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

public class MakeCJKIndex {

	private static final String index_dir = "index";

	public MakeCJKIndex(String dir_name) throws Exception {

		IndexWriter writer = new IndexWriter(index_dir, new CJKAnalyzer(), true);
		readFolder(writer, new File(dir_name));

		writer.optimize();

		writer.close();
	}

	public void readFolder(IndexWriter writer, File dir) throws Exception {
		File[] files = dir.listFiles();
		if (files == null) {
			return;
		}
		for (File file : files) {
			if (!file.exists()) {
				continue;
			} else if (file.isDirectory()) {
				readFolder(writer, file);
			} else if (file.isFile()) {
				if (getDocument(file) != null) {
					System.out.println(file.getAbsoluteFile());
					writer.addDocument(getDocument(file));
				}
			}
		}
	}

	public Document getDocument(File file) throws Exception {
		if (file.isFile() && getSuffix(file.getName()).equals("html")) {
			Document doc = new Document();
			final String path = file.getAbsolutePath();
			doc.add(new Field("path", path, Field.Store.YES, Field.Index.TOKENIZED));
			doc.add(new Field("contents", new FileReader(path)));
			return doc;
		}
		return null;
	}

	public static String getSuffix(String fileName) {
		if (fileName == null) {
			return null;
		}
		int point = fileName.lastIndexOf(".");
		if (point != -1) {
			return fileName.substring(point + 1);
		}
		return fileName;
	}

	public static void main(String[] args) throws Exception {
		MakeCJKIndex mkindex = new MakeCJKIndex(args[0]);
	}
}
