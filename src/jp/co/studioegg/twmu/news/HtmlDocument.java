package jp.co.studioegg.twmu.news;

import jp.co.studioegg.twmu.news.utils.CharsetDetector;
import java.io.File;
import java.io.IOException;
import static jp.co.studioegg.twmu.news.EggDocument.uid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.jsoup.Jsoup;

/**
 * A utility for making Lucene Documents for HTML documents.
 */
public class HtmlDocument extends EggDocument {

	private static final Log log = LogFactory.getLog(HtmlDocument.class);

	private HtmlDocument() {
	}

	public static Document Document(File f, String charset) throws IOException, InterruptedException {
		javaxt.io.File fx = new javaxt.io.File(f);
		Document doc = new Document();
		doc.add(new Field("type", "html", Field.Store.YES, Field.Index.UN_TOKENIZED));

		doc.add(new Field("filename", f.getName(), Field.Store.YES, Field.Index.UN_TOKENIZED));
		String url = HtmlDocument.config.getString("indexer.base.url") + f.getPath().substring(HtmlDocument.config.getString("indexer.html.dir").length());
		doc.add(new Field("url", url, Field.Store.YES, Field.Index.UN_TOKENIZED));
		if (fx.getCreationTime() != null) {
			doc.add(new Field("created", DateTools.timeToString(fx.getCreationTime().getTime(), DateTools.Resolution.MINUTE), Field.Store.YES, Field.Index.UN_TOKENIZED));
		} else {
			doc.add(new Field("created", DateTools.timeToString(f.lastModified(), DateTools.Resolution.MINUTE), Field.Store.YES, Field.Index.UN_TOKENIZED));
		}
		doc.add(new Field("updated", DateTools.timeToString(f.lastModified(), DateTools.Resolution.MINUTE), Field.Store.YES, Field.Index.UN_TOKENIZED));
		doc.add(new Field("uid", uid(f), Field.Store.YES, Field.Index.UN_TOKENIZED));
		if (charset == null) {
			try {
				CharsetDetector cd = new CharsetDetector(f);
				charset = cd.detect();
			} catch (Exception e) {
				e.printStackTrace();
				charset = "ISO-8859-1";
			}
		}
		org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(f, charset);
		doc.add(new Field("charset", charset, Field.Store.YES, Field.Index.UN_TOKENIZED));
		try {
			String content = jsoupDoc.select(HtmlDocument.config.getString("indexer.body.selector")).first().text();
			doc.add(new Field("content", content, Field.Store.YES, Field.Index.TOKENIZED));
		} catch (NullPointerException e) {
			doc.add(new Field("content", "NONE", Field.Store.YES, Field.Index.TOKENIZED));
			log.error("content is null : " + f.getPath());
		}

		try {
			String title = jsoupDoc.title();
			doc.add(new Field("title", title, Field.Store.YES, Field.Index.TOKENIZED));
		} catch (NullPointerException e) {
			doc.add(new Field("title", "NONE", Field.Store.YES, Field.Index.TOKENIZED));
			log.error("title is null : " + f.getPath());
		}
		try {
			String keywords = jsoupDoc.head().select("meta[name=keywords]").first().attr("content");
			doc.add(new Field("keywords", keywords, Field.Store.YES, Field.Index.UN_TOKENIZED));
		} catch (NullPointerException e) {
			doc.add(new Field("keywords", "NONE", Field.Store.YES, Field.Index.UN_TOKENIZED));
			log.error("keywords is null : " + f.getPath());
		}

		try {
			String description = jsoupDoc.head().select("meta[name=description]").first().attr("content");
			doc.add(new Field("description", description, Field.Store.YES, Field.Index.TOKENIZED));
		} catch (NullPointerException e) {
			doc.add(new Field("description", "NONE", Field.Store.YES, Field.Index.TOKENIZED));
			log.error("description is null : " + f.getPath());
		}
		return doc;
	}
}
